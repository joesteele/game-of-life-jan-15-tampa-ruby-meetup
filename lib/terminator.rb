module Terminator
  RULES = [
    # any live cell w/ <2 live neighbors dies
    Rule.new do |cell, grid|
      !(grid.alive_neighbors_for(cell).size < 2)
    end,
    # any live cell w/ 2 or 3 live neighbors lives
    Rule.new do |cell, grid|
      [2, 3].include?(grid.alive_neighbors_for(cell).size)
    end,
    # any live cell w/ > 3 live neigbors dies
    Rule.new do |cell, grid|
      !(grid.alive_neighbors_for(cell).size > 3)
    end,
    # any dead cell w/ 3 live neighbors lives
    Rule.new(false) do |cell, grid|
      grid.alive_neighbors_for(cell).size == 3
    end
  ]

  def self.determine_life(cell, grid)
    new_life = RULES.all? { |rule| rule.lives?(cell, grid) }
    Grid::Cell.new(cell.x, cell.y, new_life)
  end
end

class Rule
  attr_reader :alive_condition, :condition

  def initialize(alive_condition=true, &condition)
    @alive_condition = alive_condition
    @condition = condition
  end

  def lives?(cell, grid)
    if cell.alive? == alive_condition
      return true # test doesn't apply to this state; pass
    end

    condition.call(cell, grid)
  end
end

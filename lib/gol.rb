require_relative 'grid'
require_relative 'rule'
require_relative 'terminator'

module Gol
  def self.run(iterations, size)
    grid = Grid.new size

    puts "\nGame of Life\n\n"

    render grid

    iterations.times do
      puts "\n\n----------------------------\n\n"
      grid = Grid.new(grid.size, grid.cells.map {|cell|
        Terminator.determine_life(cell, grid)
      })
      render grid
    end
  end

  def self.render(grid)
    rows = grid.size.times.map do |i|
      grid.cells[i*grid.size...(i+1)*grid.size].map do |cell|
        cell.alive? ? 'X' : ' '
      end
    end

    print rows.map {|row| row.join("") }.join("\n")
    print "\n"
  end
end

class Grid
  DEFAULT_SIZE = 9
  ADJ_COORDS = [-1, 0, 1]

  Cell = Struct.new(:x, :y, :alive) do
    def alive?
      alive
    end
  end

  attr_reader :size, :cells

  def initialize(size=DEFAULT_SIZE, seed=[])
    @size = size
    @cells = seed.empty? ? generate_seed : seed
  end

  def generate_seed
    (size*size).times.map { |i|
      Cell.new(i / size, i % size, rand(2) == 1)
    }
  end

  def alive_neighbors_for(cell)
    neighbors_for(cell).select {|n| n.alive? }
  end

  def neighbors_for(cell)
    cells.select {|c|
      ADJ_COORDS.include?(cell.x - c.x) &&
        ADJ_COORDS.include?(cell.y - c.y) &&
        !(c.x == cell.x && c.y == cell.y)
    }
  end
end

require './lib/gol'

describe do
  it 'has a grid' do
    grid = Grid.new
    neighbors = grid.neighbors_for(Grid::Cell.new(1, 1, true))
    expect(neighbors.size).to be(8)
  end

  it 'alive neighbors' do
    cells = [
      Grid::Cell.new(0,0,true),
      Grid::Cell.new(0,1,true),
      Grid::Cell.new(0,2,true),
      Grid::Cell.new(0,3,true),
      Grid::Cell.new(1,0,true),
      Grid::Cell.new(1,1,true),
      Grid::Cell.new(1,2,true),
      Grid::Cell.new(1,3,true),
      Grid::Cell.new(2,0,true),
      Grid::Cell.new(2,1,true),
      Grid::Cell.new(2,2,true),
      Grid::Cell.new(0,3,false),
      Grid::Cell.new(1,3,false),
      Grid::Cell.new(2,3,false)
    ]

    grid = Grid.new 4, cells
    neighbors = grid.alive_neighbors_for(Grid::Cell.new(1, 1, true))
    expect(neighbors.size).to be(8)
  end

  it 'runs' do
    Gol.run(5, 9)
  end
end
